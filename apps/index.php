<?php
include $_SERVER['DOCUMENT_ROOT'] . "/src/main.php";
$app = new App("apps", "Peter Kerschl's Webapps");
if ($app->check_user_login(true)) {
?>
	<article>
		<h1><?= $app->get_app_title() ?></h1>
		<hr />
		<p><a href="/">index</a></p>
		<?php
		$sql = "SELECT `App`.`AppName` FROM `UserApp` INNER JOIN `App` ON `UserApp`.`AppID` = `App`.`AppID` " .
			"WHERE `UserApp`.`UserID` = " . $_SESSION['UserID'] . " ORDER BY `App`.`AppName`";
		$apps = mysqli_query($app->database(), $sql);
		if (mysqli_num_rows($apps) > 0) {
			while ($a = mysqli_fetch_array($apps)) { ?>
				<p><a href="/<?= $a["AppName"] ?>/"><?= $a["AppName"] ?></a></p>
		<?php }
		} ?>
		<p>
			<?php include $_SERVER['DOCUMENT_ROOT'] . "/assets/icons/external-link.svg"; ?>
			<a href="https://gitlab.com/kerschl" target="_blank">gitlab</a>
		</p>
		<p>
			<?php include $_SERVER['DOCUMENT_ROOT'] . "/assets/icons/external-link.svg"; ?>
			<a href="https://cloud.pmke.de/" target="_blank">nextcloud</a>
		</p>
		<p><a href="/src/auth.php">logout</a></p>
	</article>
<?php
}
