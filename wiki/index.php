<?php
include $_SERVER['DOCUMENT_ROOT'] . "/src/main.php";
$app = new App("wiki", "pmke.de/wiki");
if ($app->check_user_login()) {
	if (isset($_POST['new'])) {
		$newtitle = mysqli_real_escape_string($app->database(), htmlspecialchars($_POST['newtitle']));
		$sql = "INSERT INTO `Wiki` (`Title`, `Public`, `Content`, `CreateUser`, `UpdateUser`) VALUES ('" . $newtitle . "','0','new Wiki','" . $_SESSION['UserID'] . "','" . $_SESSION['UserID'] . "')";
		if (!mysqli_query($app->database(), $sql)) {
			echo "Error: " . $sql . "<br>" . mysqli_error($app->database());
		}
	}
	$title = "";
	$content = "";
	$where = "";
	if (isset($_POST['filter'])) {
		if (isset($_POST['title']) && !empty($_POST['title'])) {
			$title = $_POST['title'];
			$where = $where . " And `Title` Like '%" . $title . "%'";
		}
		if (isset($_POST['content']) && !empty($_POST['content'])) {
			$content = $_POST['content'];
			$where = $where . " And `Content` Like '%" . $content . "%'";
		}
	}
	$sql = "SELECT `WikiID`, `Title`, `Public`, `Created`, `Updated` FROM `Wiki` WHERE (`CreateUser` = '" .
		$_SESSION['UserID'] . "' OR `UpdateUser` = '" . $_SESSION['UserID'] . "' OR `Public` = 1)" .
		$where . " GROUP BY `Wiki`.`WikiID` ORDER BY `Wiki`.`Updated` DESC";
	$result = mysqli_query($app->database(), $sql);
?>
	<article>
		<h1>Wiki</h1>
		<hr />
		<form id="search" action="/wiki/" method="post">
			<p>
				<label for="title">Titel:</label>
				<input id="title" type="text" name="title" autofocus value="<?= $title ?>" class="bb-dotted" />
			</p>
			<p>
				<label for="content">Fulltext:</label>
				<input id="content" type="text" name="content" value="<?= $content ?>" class="bb-dotted" />
			</p>
			<input type="submit" name="filter" style="display: none" />
		</form>
	</article>
	<table>
		<?php
		if (mysqli_num_rows($result) > 0) {
			while ($wiki = mysqli_fetch_array($result)) { ?>
				<tr class="clickable" tabindex="0" <?= "onclick=\"window.location.href='/wiki/" . $wiki["WikiID"] . "';\" onKeypress=\"window.location.href='/wiki/" . $wiki["WikiID"] . "';\"" ?>>
					<td><?= htmlspecialchars_decode($wiki["Title"]) ?></td>
					<td class="small mobile-hidden">created: <?= $wiki["Created"] ?><br>updated: <?= $wiki["Updated"] ?></td>
				</tr>
			<?php }
		} else { ?>
			<tr>
				<td colspan="2">keine Suchergebnisse</td>
			</tr>
		<?php }
		?>
		<tr>
			<td id="new-wiki" colspan="2">
				<form class="flex" action="/wiki/" method="post">
					<input type="text" name="newtitle" required class="bb-dotted"></input>
					<button tabindex="-1" type="submit" name="new">ADD</button>
				</form>
			</td>
		</tr>
	</table>
	<aside>
		<p><a href="/">index</a></p>
		<p><a href="/apps/">apps</a></p>
	</aside>
	<style>
		#new-wiki {
			padding: 0.7rem 1rem;
		}

		#new-wiki input {
			width: 100%;
			margin-right: 1rem;
		}
	</style>
<?php
}
