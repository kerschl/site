<?php
$wiki_id = str_replace(['/', 'wiki'], '', $_SERVER['REQUEST_URI']); // get wiki_id
if (empty($wiki_id) && !preg_match("/^[0-9]+$/", $wiki_id)) {
	header("Location: /wiki/", true, 302);
	exit();
}
// if id is set and matches pattern
include $_SERVER['DOCUMENT_ROOT'] . "/src/main.php";
$app = new App("wiki", "pmke.de/wiki", "/css/wiki.css");
$app->make_database();
$authcheck = $app->check_user();
$sql = "SELECT `WikiID`, `Title`, `Content`, `Public`, `Created`, `Updated` FROM `Wiki` WHERE `WikiID` = '" . $wiki_id . "'"; // sql by wiki_id
$result = mysqli_query($app->database(), $sql); // get entries
if (mysqli_num_rows($result) == 1) { // if one result
	while ($wiki = mysqli_fetch_array($result)) {
		if ($authcheck || $wiki['Public'] == 1) { // show result if logged in or public 
?>
			<header>
				<h1><?= htmlspecialchars_decode($wiki['Title']) ?></h1>
				<hr>
			</header>
			<article id="markdown" class="mx-auto">
			</article>
			<script src="/lib/markdown-it.js"></script>
			<script>
				var el = document.getElementById('markdown');
				var str = <?= json_encode(htmlspecialchars_decode($wiki['Content'])) ?>;
				el.innerHTML = el.innerHTML + markdownit().render(str);
			</script>
			<aside>
				<hr>
				<div class="flex">
					<div>
						<button class="button mr-auto print-hidden" onclick="window.location.href='/wiki';">Back</button>
					</div>
					<span class="small mobile-hidden">created: <?= $wiki['Created'] ?> - updated: <?= $wiki['Updated'] ?></span>
					<div>
						<button class="button ml-auto print-hidden" onclick="window.location.href='/wiki/edit.php/<?= $wiki['WikiID'] ?>';">Edit</button>
					</div>
				</div>
			</aside>
<?php
		} else {
			$app->check_user_login();
		}
	}
} else {
	header("Location: /wiki/", true, 302);
	exit();
}
