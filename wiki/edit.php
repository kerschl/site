<?php
include $_SERVER['DOCUMENT_ROOT'] . "/src/main.php";
$app = new App("wiki", "pmke.de/wiki/edit", "/css/wiki.css");
if ($app->check_user_login()) {
	$wiki_id = str_replace(['/', 'wiki', 'edit.php'], '', $_SERVER['REQUEST_URI']);
	if (isset($_POST['back'])) { // back
		header("Location: /wiki/" . $wiki_id, true, 302);
		exit();
	}
	if (isset($_POST['update'])) { // handle updates
		$content = mysqli_real_escape_string($app->database(), htmlspecialchars($_POST['content']));
		$sql = "UPDATE `Wiki` SET `Content` = '" . $content . "', Updated = now() WHERE `WikiID` = '" . $_POST['id'] . "'";
		if (!mysqli_query($app->database(), $sql)) {
			echo "Error: " . $sql . "<br>" . mysqli_error($app->database());
		}
	}
	$sql = "SELECT `WikiID`, `Title`, `Content`, `Created`, `Updated` FROM `Wiki` WHERE `WikiID` = '" . $wiki_id . "'"; // sql by id
	$result = mysqli_query($app->database(), $sql);
	while ($wiki = mysqli_fetch_array($result)) { // show result 
?>
		<form action="" method="post">
			<header>
				<h1><?= htmlspecialchars_decode($wiki['Title']) ?></h1>
				<hr>
			</header>
			<article id="markdown">
				<div>
					<input type="hidden" name="id" value="<?= $wiki['WikiID'] ?>">
					<textarea name="content" class="textarea"><?= htmlspecialchars_decode($wiki['Content']) ?></textarea>
				</div>
			</article>
			<aside>
				<hr>
				<div class="flex">
					<div>
						<button class="button mr-auto" type="submit" name="back">Back</button>
					</div>
					<span class="small mobile-hidden">created: <?= $wiki['Created'] ?> - updated: <?= $wiki['Updated'] ?></span>
					<div>
						<button class="button ml-auto" type="submit" name="update">Save</button>
					</div>
				</div>
			</aside>
			<script>
				document.querySelector("textarea.textarea").addEventListener("input", event => {
					document.querySelector("span.small").innerText = "characters: " + event.currentTarget.value.length + "/9000"
				});
			</script>
		</form>
<?php
	}
}
