<?php
$webcam_id = str_replace(['api', 'wallpaper', '/'], '', $_SERVER['REQUEST_URI']); // get webcam_id
if (empty($webcam_id) && !preg_match("/^[0-9a-x\-]+$/", $webcam_id)) {
	die("404");
}

include $_SERVER['DOCUMENT_ROOT'] . '/src/config/sql.php';
try {
	$conn = mysqli_connect($sql_host, $sql_username, $sql_password, $sql_database);
} catch (mysqli_sql_exception $e) {
	die("SQL error: " . $e->getMessage());
}

$sql = "SELECT `MapContent` FROM `MapString` WHERE `MapTable` = 'webcam' AND `MapKey` = '" . $webcam_id . "';";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) != 1) {
	die("404");
}

while ($webcam = mysqli_fetch_array($result)) {
	$config = explode("#", $webcam["MapContent"]);
	$filePath = "/api/wallpaper/";

	date_default_timezone_set("Europe/Berlin");
	$datetime = new DateTime();

	while ($datetime->format('i') % 10 != 0) {
		$datetime->modify('-1 minute');
	}

	for ($i = 0; $i < 6; $i++) { // maximum 1h
		$local_filename = $config[1] . date_format($datetime, "_Y-m-d_Hi") . $config[3] . "." . $config[4];
		$local_file = $_SERVER['DOCUMENT_ROOT'] . $filePath . $local_filename;
		$local_url = "https://" . $_SERVER['HTTP_HOST'] . $filePath . $local_filename;
		$remote_url = $config[0] . "/" . $config[1] . "/" . date_format($datetime, $config[2]) . $config[3] . "." . $config[4];

		if (file_exists($local_file)) { // if local file exists
			header("Content-Type: application/json");
			echo json_encode(array(
				'data' => array(
					'current' => $local_url,
					'datetime' => date_format($datetime, "Y.m.d H:i")
				)
			));
			mysqli_close($conn);
			exit;
		}

		if (get_headers($remote_url, 1)[0] == "HTTP/1.1 200 OK") { // if new remote file exists
			$oldFiles = glob($_SERVER['DOCUMENT_ROOT'] . $filePath . $config[1] . "*");
			foreach ($oldFiles as $file) {
				if (is_file($file)) {
					unlink($file);
				}
			}

			shell_exec("wget \"$remote_url\" -O \"$local_file\"");

			header("Content-Type: application/json");
			echo json_encode(array(
				'data' => array(
					'current' => $local_url,
					'origin' => $remote_url,
					'datetime' => date_format($datetime, "Y.m.d H:i")
				)
			));
			mysqli_close($conn);
			exit;
		}
		$datetime->modify('-10 minutes');
	};

	$ls = str_replace("\n", "", shell_exec("(cd " . $_SERVER['DOCUMENT_ROOT'] . $filePath . " && ls " . $config[1] . "*)"));
	if (is_string($ls)) { // last resort - cache
		header("Content-Type: application/json");
		echo json_encode(array(
			'data' => array(
				'current' => "https://" . $_SERVER['HTTP_HOST'] . $filePath . $ls,
				'datetime' => "unknown"
			)
		));
		mysqli_close($conn);
		exit;
	}
}
die("404");
