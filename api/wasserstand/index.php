<?php
if ($_SERVER['REQUEST_URI'] != "/api/wasserstand/prien.json") {
	die("404");
}

$fileData = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/api/wasserstand/cache.json');
if ($fileData == false) {
	main();
} else {
	$arr = json_decode($fileData, true);
	if (DateTime::createFromFormat("d.m.Y H:i", $arr['DateTime']) < (new DateTime())->modify('-59 minutes')) {
		main();
	} else {
		printJSON($arr, true);
	}
}

function main()
{
	// https://www.gkd.bayern.de/de/seen/wasserstand/isar/stock-18400503/messwerte/tabelle?beginn=22.04.2024&ende=23.04.2024
	$url = 'https://www.gkd.bayern.de/de/seen/wasserstand/isar/stock-18400503/messwerte/tabelle?' .
		'beginn=' . (new DateTime())->modify('-59 minutes')->format("d.m.Y") . '&ende=' . (new DateTime())->format("d.m.Y");
	$html = file_get_contents($url);
	if ($html === false) {
		die('Error fetching remote HTML');
	}
	$arr = array();

	preg_match_all("/>([0-9]*\,[0-9]*)</", $html, $matches);
	if (preg_last_error() !== PREG_NO_ERROR) {
		die(preg_last_error_msg());
	}
	$arr['WasserstandInMetern'] = $matches[1][0];

	preg_match_all("/>([0-9]*\.[0-9]*\.[0-9]* [0-9]*:[0-9]*)</", $html, $dateString);
	if (preg_last_error() !== PREG_NO_ERROR) {
		die(preg_last_error_msg());
	}
	$arr['DateTime'] = $dateString[1][0];

	printJSON($arr, false);
	file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/api/wasserstand/cache.json', json_encode($arr));
}

function printJSON($arr, $cached)
{
	header("Content-Type: application/json");
	echo json_encode(array(
		'data' => array(
			'current' => $arr['WasserstandInMetern'],
			'unit' => "Meter",
			'location' => "Prien Stock",
			'datetime' => (DateTime::createFromFormat("d.m.Y H:i", $arr['DateTime']))->format('Y-m-d H:i'),
			'cached' => $cached
		)
	));
}
