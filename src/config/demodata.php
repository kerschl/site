<?php
include $_SERVER['DOCUMENT_ROOT'] . '/src/config/sql.php';
$conn = mysqli_connect($sql_host, $sql_username, $sql_password, $sql_database);
if (!$conn) {
	die("Connection failed: " . mysqli_connect_error());
}

$result = $conn->query("SELECT * FROM Person;");
if ($result->num_rows == 0) { // test if data exists
	$sql = array(
		"INSERT INTO Person (FirstName,LastName,CreateUser,UpdateUser) VALUES ('Peter','Kerschl',1,1);",
		"INSERT INTO User (PersonID,Username,PasswordHash,Active,CreateUser,UpdateUser) VALUES (1,'peter','',1,1,1);",
		"INSERT INTO User (PersonID,Username,PasswordHash,Active,CreateUser,UpdateUser) VALUES (1,'guest','',1,1,1);",
		"INSERT INTO App (AppName,CreateUser,UpdateUser) VALUES ('notes',1,1);",
		"INSERT INTO App (AppName,CreateUser,UpdateUser) VALUES ('stats',1,1);",
		"INSERT INTO App (AppName,CreateUser,UpdateUser) VALUES ('upload',1,1);",
		"INSERT INTO App (AppName,CreateUser,UpdateUser) VALUES ('wiki',1,1);",
		"INSERT INTO App (AppName,CreateUser,UpdateUser) VALUES ('apps',1,1);",
		"INSERT INTO UserApp (UserID,AppID,CreateUser,UpdateUser) VALUES (1,1,1,1);",
		"INSERT INTO UserApp (UserID,AppID,CreateUser,UpdateUser) VALUES (2,2,1,1);",
		"INSERT INTO UserApp (UserID,AppID,CreateUser,UpdateUser) VALUES (1,2,1,1);",
		"INSERT INTO UserApp (UserID,AppID,CreateUser,UpdateUser) VALUES (1,3,1,1);",
		"INSERT INTO UserApp (UserID,AppID,CreateUser,UpdateUser) VALUES (1,4,1,1);",
		"INSERT INTO UserApp (UserID,AppID,CreateUser,UpdateUser) VALUES (1,5,1,1);",
		"INSERT INTO MapNumeric (MapTable,MapKey,MapContent,CreateUser,UpdateUser) VALUES ('fritzlog',10,'Kerschl-FritzBox',1,1);",
		"INSERT INTO MapNumeric (MapTable,MapKey,MapContent,CreateUser,UpdateUser) VALUES ('fritzlog',11,'Alex-FritzBox',1,1);",
		"INSERT INTO MapNumeric (MapTable,MapKey,MapContent,CreateUser,UpdateUser) VALUES ('fritzlog',12,'Asam-FritzBox',1,1);",
		"INSERT INTO MapNumeric (MapTable,MapKey,MapContent,CreateUser,UpdateUser) VALUES ('fritzlog',13,'Schmied-Lan',1,1);",
		"INSERT INTO MapNumeric (MapTable,MapKey,MapContent,CreateUser,UpdateUser) VALUES ('fritzlog',14,'Gretschmal-FritzBox',1,1);",
		"INSERT INTO MapNumeric (MapTable,MapKey,MapContent,CreateUser,UpdateUser) VALUES ('fritzlog_color',10,'#547496',1,1);",
		"INSERT INTO MapNumeric (MapTable,MapKey,MapContent,CreateUser,UpdateUser) VALUES ('fritzlog_color',11,'#643a7a',1,1);",
		"INSERT INTO MapNumeric (MapTable,MapKey,MapContent,CreateUser,UpdateUser) VALUES ('fritzlog_color',12,'#d11149',1,1);",
		"INSERT INTO MapNumeric (MapTable,MapKey,MapContent,CreateUser,UpdateUser) VALUES ('fritzlog_color',13,'#1a8fe3',1,1);",
		"INSERT INTO MapNumeric (MapTable,MapKey,MapContent,CreateUser,UpdateUser) VALUES ('fritzlog_color',14,'#f17105',1,1);",
		"INSERT INTO MapNumeric (MapTable,MapKey,MapContent,CreateUser,UpdateUser) VALUES ('fritzlog_color',15,'#6610f2',1,1);",
		"INSERT INTO MapNumeric (MapTable,MapKey,MapContent,CreateUser,UpdateUser) VALUES ('fritzlog_color',16,'#e6c229',1,1);",
		"INSERT INTO MapString (MapTable,MapKey,MapContent,CreateUser,UpdateUser) VALUES ('webcam','kampenwand','https://www.foto-webcam.eu/webcam#kampenwand#Y/m/d/Hi#_uh#jpg',1,1);",
		"INSERT INTO MapString (MapTable,MapKey,MapContent,CreateUser,UpdateUser) VALUES ('webcam','hochries-ost','https://www.foto-webcam.eu/webcam#hochries-ost#Y/m/d/Hi#_uh#jpg',1,1);",
		"INSERT INTO MapString (MapTable,MapKey,MapContent,CreateUser,UpdateUser) VALUES ('webcam','tum-olympiapark','https://www.foto-webcam.eu/webcam#tum-olympiapark#Y/m/d/Hi#_uh#jpg',1,1);",
		"INSERT INTO MapString (MapTable,MapKey,MapContent,CreateUser,UpdateUser) VALUES ('webcam','bad-endorf','https://www.foto-webcam.eu/webcam#bad-endorf#Y/m/d/Hi#_uh#jpg',1,1);",
		"INSERT INTO MapString (MapTable,MapKey,MapContent,CreateUser,UpdateUser) VALUES ('webcam','rosenheim','https://www.foto-webcam.eu/webcam#rosenheim#Y/m/d/Hi#_uh#jpg',1,1);",
		"INSERT INTO MapString (MapTable,MapKey,MapContent,CreateUser,UpdateUser) VALUES ('webcam','chiemsee','https://www.terra-hd.de#chiemsee#Y/m/d/Hi#l#jpg',1,1);",
		"INSERT INTO MapString (MapTable,MapKey,MapContent,CreateUser,UpdateUser) VALUES ('webcam','prienavera','https://www.terra-hd.de#prienavera#Y/m/d/Hi#l#jpg',1,1);",
		"INSERT INTO MapString (MapTable,MapKey,MapContent,CreateUser,UpdateUser) VALUES ('webcam','bernau-felden','https://www.terra-hd.de#bernau-felden#Y/m/d/Hi#l#jpg',1,1);",
		"INSERT INTO MapString (MapTable,MapKey,MapContent,CreateUser,UpdateUser) VALUES ('webcam','deutschesmuseum2','https://www.terra-hd.de#deutschesmuseum2#Y/m/d/Hi#l#jpg',1,1);",
		"INSERT INTO MapString (MapTable,MapKey,MapContent,CreateUser,UpdateUser) VALUES ('webcam','domberg1','https://www.terra-hd.de#domberg1#Y/m/d/Hi#l#jpg',1,1);",
	);
	foreach ($sql as $s) {
		if (!mysqli_query($conn, $s)) {
			echo "Error: " . $sql . "<br>" . mysqli_error($app->database());
		}
	}
}
