<?php
include "./sql-from-hell/SQL-from-Hell.php";
include "./sql.php";
$varchar = "VARCHAR(255)";
$datetime = "DATETIME";
$int = "INT";
$text = "TEXT";

/////////////////////////
// scheme defined here // 
/////////////////////////

$db = new SQLfromHell($sql_database, $sql_host, $sql_username, $sql_password);

$db->AddTable('Person', new Table("Person", "PersonID"));
$db->tables['Person']->AddColumn("FirstName", $varchar, true); // nullable = true
$db->tables['Person']->AddColumn("LastName", $varchar, false); // nullable = false

$db->AddTable('User', new Table("User", "UserID"));
$db->tables['User']->AddForeignKey($db->tables['Person']->GetPrimaryKey(), false, true); // nullable, cascade on delete = true
$db->tables['User']->AddColumn("Username", $varchar, false);
$db->tables['User']->AddColumn("PasswordHash", $varchar, true);
$db->tables['User']->AddColumn("Active", $int, false);

$db->AddTable('App', new Table("App", "AppID"));
$db->tables['App']->AddColumn("AppName", $varchar, false);
$db->tables['App']->AddConstraint("UniqueApp", "UNIQUE(`AppName`)"); // add constraint 'UniqueApp' to avoide duplicates

$db->AddTable('UserApp', new Table("UserApp", "ID"));
$db->tables['UserApp']->AddForeignKey($db->tables['User']->GetPrimaryKey(), false, true); // nullable, cascade on delete = true
$db->tables['UserApp']->AddForeignKey($db->tables['App']->GetPrimaryKey(), false, true); // nullable, cascade on delete = true
$db->tables['UserApp']->AddConstraint("UniqueUserApp", "UNIQUE(" .
	"`" . $db->tables['User']->GetPrimaryKey()["key"] . "`," .
	"`" . $db->tables['App']->GetPrimaryKey()["key"] . "`)"); // no duplicates

$db->AddTable('MapNumeric', new Table("MapNumeric", "ID"));
$db->tables['MapNumeric']->AddColumn("MapTable", $varchar, false);
$db->tables['MapNumeric']->AddColumn("MapKey", $int, false);
$db->tables['MapNumeric']->AddColumn("MapContent", $varchar, true);
$db->tables['MapNumeric']->AddConstraint("UniqueMapNumeric", "UNIQUE(`MapTable`, `MapKey`)"); // uniqueness

$db->AddTable('MapString', new Table("MapString", "ID"));
$db->tables['MapString']->AddColumn("MapTable", $varchar, false);
$db->tables['MapString']->AddColumn("MapKey", $varchar, false);
$db->tables['MapString']->AddColumn("MapContent", $varchar, true);
$db->tables['MapString']->AddConstraint("UniqueMapString", "UNIQUE(`MapTable`, `MapKey`)"); // uniqueness

$db->AddTable('FritzLog', new Table("FritzLog", "LogID"));
$db->tables['FritzLog']->AddColumn("FritzBox", $int, true);
$db->tables['FritzLog']->AddColumn("IpAddress", $int, true);
$db->tables['FritzLog']->AddColumn("LogDateTime", $datetime, true);
$db->tables['FritzLog']->AddConstraint("UniqueFritzLog", "UNIQUE(`FritzBox`, `IpAddress`, `LogDateTime`)"); // no duplicates

$db->AddTable('Note', new Table("Note", "NoteID"));
$db->tables['Note']->AddColumn("Content", $text, false);

$db->AddTable('Wiki', new Table("Wiki", "WikiID"));
$db->tables['Wiki']->AddColumn("Title", $varchar, false);
$db->tables['Wiki']->AddColumn("Content", $text, true);
$db->tables['Wiki']->AddColumn("Public", $int, false);

$db->AddTable('Webcam', new Table("Webcam", "StampID"));
$db->tables['Webcam']->AddColumn("Location", $varchar, true);
$db->tables['Webcam']->AddColumn("Stamp", $datetime, true);
$db->tables['Webcam']->AddConstraint("UniqueWebcam", "UNIQUE(`Location`, `Stamp`)"); // no duplicates

$db->MakeDB();

////////////////
// end scheme //
////////////////
