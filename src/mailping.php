<?php
// PING
// Peter's Interessante Nachrichten Generator

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require $_SERVER['DOCUMENT_ROOT'] . '/lib/PHPMailer/Exception.php';
require $_SERVER['DOCUMENT_ROOT'] . '/lib/PHPMailer/PHPMailer.php';
require $_SERVER['DOCUMENT_ROOT'] . '/lib/PHPMailer/SMTP.php';

function MailPing($subject, $body)
{
	$body = $body . "<br/><br/>Diese Nachricht wurde automatisch erstellt von P.I.N.G.<br/>Peter's interessante Nachrichten Generator";
	// Create an instance; passing `true` enables exceptions
	$mail = new PHPMailer(true);
	try {
		include $_SERVER['DOCUMENT_ROOT'] . '/src/config/imap.php';
		// Server settings
		$mail->SMTPDebug = SMTP::DEBUG_SERVER;				// Enable verbose debug output
		$mail->isSMTP();									// Send using SMTP
		$mail->Host       = $imap_host;						// Set the SMTP server to send through
		$mail->SMTPAuth   = true;							// Enable SMTP authentication
		$mail->Username   = $imap_username;					// SMTP username
		$mail->Password   = $imap_password;					// SMTP password
		$mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;	// Enable implicit TLS encryption
		$mail->Port       = 465;							// TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`
		$mail->setFrom('service@pmke.de');
		$mail->addReplyTo('service@pmke.de');
		$mail->addAddress('peter@pmke.de');
		$mail->isHTML(true);								// Set email format to HTML
		$mail->Subject = $subject;
		$mail->Body    = $body;
		$mail->AltBody = strip_tags(str_replace(["<br>", "<br/>"], "\n", $body));
		$mail->send();
		echo "done";
	} catch (Exception $e) {
		echo "Mailer Error: {$mail->ErrorInfo}";
	}
}
