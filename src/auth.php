<?php
session_start();
if (isset($_POST['username'], $_POST['password'], $_POST['uri'])) {
	$username = preg_replace('/[^a-zA-Z0-9]/', '', $_POST['username']);
	if ($username == $_POST['username']) { // filter spam
		include $_SERVER['DOCUMENT_ROOT'] . '/src/config/sql.php';
		$conn = mysqli_connect($sql_host, $sql_username, $sql_password, $sql_database);
		if (!$conn) {
			die("Connection failed: " . mysqli_connect_error());
		}
		$sql = "SELECT `PasswordHash`, `UserID` FROM `User` Where `Username` = '" . $username . "';";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while ($row = $result->fetch_assoc()) {
				if (password_verify($_POST['password'], $row['PasswordHash'])) {
					$_SESSION['Username'] = $username; // user is now logged in
					$_SESSION['UserID'] = $row['UserID'];
				}
			}
			header("Location: " . $_POST['uri'], true, 302);
			exit();
		}
	}
}
session_destroy();
header("Location: /", true, 302);
exit();

// create password with //echo password_hash('password', PASSWORD_BCRYPT);
