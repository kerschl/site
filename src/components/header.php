<!DOCTYPE html>
<html lang="de">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php echo $this->app_title ?? "pmke.de" ?></title>
	<link rel="stylesheet" href="/css/01-normalize.css" />
	<link rel="stylesheet" href="/css/02-root.css" />
	<link rel="stylesheet" href="/css/03-elements.css" />
	<link rel="stylesheet" href="/css/04-styles.css" />
	<link rel="stylesheet" href="/css/09-print.css" />
	<?php if (!is_null($style_sheet)) { // optional additional styles
		echo "<link rel=\"stylesheet\" href=\"" . $style_sheet . "\" />";
	} ?>
	<link rel="icon" type="image/x-icon" href="/favicon.ico" />
	<link rel="manifest" href="manifest.json" />
</head>

<body>
	<main>
