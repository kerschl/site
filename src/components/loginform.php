<article>
	<h1>Login</h1>
	<form id="login" action="/src/auth.php" method="post">
		<p>
			<label for="username">Username:</label>
			<select name="username" id="username">
				<?php foreach ($user as $u) {
					echo '<option value="' . $u . '">' . $u . '</option>';
				} ?>
			</select>
		</p>
		<p>
			<label for="password">Password:</label>
			<input id="password" type="password" name="password" autofocus class="bb-dotted" />
		</p>
		<input type="hidden" id="uri" name="uri" value="<?= $_SERVER['REQUEST_URI'] ?>">
	</form>
</article>
