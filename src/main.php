<?php
class Site
{
	protected $app_name;
	protected $app_title;

	public function __construct($app_name, $app_title, $style_sheet = null)
	{
		$this->app_name = $app_name;
		$this->app_title = $app_title;
		include $_SERVER['DOCUMENT_ROOT'] . "/src/components/header.php";
	}

	public function get_app_name(): string
	{
		return $this->app_name;
	}

	public function get_app_title(): string
	{
		return $this->app_title;
	}

	public function __destruct()
	{
		include $_SERVER['DOCUMENT_ROOT'] . "/src/components/footer.php";
	}
}

class App extends Site
{
	private mysqli $conn;

	public function __construct($app_name, $app_title, $style_sheet = null)
	{
		session_start();
		if (session_id() == "APISession") {
			// The APISession is used to cache non user-specific values
			// Apps must not be able to use this session
			session_regenerate_id(); // new random id
			session_reset(); // reset data
		}
		parent::__construct($app_name, $app_title, $style_sheet);
	}

	public function check_user($allow_all = false): bool
	{
		if (isset($_SESSION['Username'])) { // if session has user
			$this->make_database();
			if ($allow_all) {
				return true;
			}
			$sql = "SELECT User.UserName, App.AppName FROM UserApp " .
				"INNER JOIN User ON UserApp.UserID = User.UserID " .
				"INNER JOIN App ON UserApp.AppID = App.AppID " .
				"WHERE User.UserName = '" . $_SESSION['Username'] . "' " .
				"And App.AppName = '" . $this->app_name . "';";
			$result = $this->conn->query($sql);
			if ($result->num_rows > 0) {
				return true; // exit; user has access
			}
		}
		return false;
	}

	public function check_user_login($allow_all = false): bool
	{
		if (!$this->check_user($allow_all)) { // show loginform
			$this->make_database();
			$user = array(); // dropdown possible users
			if ($allow_all) {
				$sql = "SELECT User.UserName FROM User Order By User.UserID;";
			} else {
				$sql = "SELECT User.UserName FROM UserApp " .
					"INNER JOIN User ON UserApp.UserID = User.UserID " .
					"INNER JOIN App ON UserApp.AppID = App.AppID " .
					"WHERE App.AppName = '" . $this->app_name . "' Order By UserApp.ID;";
			}
			$result = $this->conn->query($sql);
			if ($result->num_rows > 0) {
				while ($row = $result->fetch_assoc()) {
					array_push($user, $row["UserName"]);
				}
			}
			include $_SERVER['DOCUMENT_ROOT'] . "/src/components/loginform.php";
			return false; // exit; user does not have access
		}
		return true;
	}

	public function make_database()
	{
		if (!isset($this->conn)) { // intended to be called but not executed multiple times
			include $_SERVER['DOCUMENT_ROOT'] . '/src/config/sql.php';
			try {
				$this->conn = mysqli_connect($sql_host, $sql_username, $sql_password, $sql_database);
			} catch (mysqli_sql_exception $e) {
				die("Connection failed: " . $e->getMessage());
			}
		}
	}

	public function database(): mysqli
	{
		return $this->conn;
	}

	public function __destruct()
	{
		if (isset($this->conn)) {
			$this->conn->close();
		}
		parent::__destruct();
	}
}
