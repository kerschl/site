<?php
include $_SERVER['DOCUMENT_ROOT'] . "/src/main.php";
$app = new App("upload", "pmke.de/upload");
if ($app->check_user_login()) {
?>
	<article>
		<?php
		if ($_FILES) {
			$target_file = "files/" . basename($_FILES["image"]["name"]);
			$uploadOk = 1;
			$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
			if (isset($_POST["submit"])) {
				$check = getimagesize($_FILES["image"]["tmp_name"]);
				if ($check !== false) {
					echo "File is an image - " . $check["mime"] . ".";
					$uploadOk = 1;
				} else {
					echo "File is not an image.";
					$uploadOk = 0;
				}
			}
			if (file_exists($target_file)) {
				echo "Sorry, file already exists.";
				$uploadOk = 0;
			}
			if ($_FILES["image"]["size"] > 4000000) {
				echo "Sorry, your file is too large.";
				$uploadOk = 0;
			}
			if (
				$imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif"
			) {
				echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
				$uploadOk = 0;
			}
			if ($uploadOk == 0) {
				echo "Your file was not uploaded.";
			} else {
				if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
					echo "The file " . htmlspecialchars(basename($_FILES["image"]["name"])) . " has been uploaded.";
				} else {
					echo "Sorry, there was an error uploading your file.";
				}
			}
		}
		?>
		<h1>pmke.de/upload</h1>
		<form action="" method="post" enctype="multipart/form-data">
			<p>
				<label for="image" class="upload">
					<span>Drop file here</span>
					<input type="file" id="image" name="image" accept="image/*" required="">
				</label>
			</p>
			<p>
				<button type="submit">UPLOAD</button>
			</p>
		</form>
		<p>
			<?php
			$path = "files/";
			if (is_dir($path)) {
				$handle = opendir($path);
				while (($file = readdir($handle)) !== false) {
					if (($file == '.') || ($file == '..') || ($file == '.gitignore')) {
						continue;
					}
					$link = "/upload/files/" . $file;
					echo "<a href=\"" . $link . "\">" . $link . "</a><br/>";
				}
				closedir($handle);
			}
			?>
		</p>
	</article>
	<style>
		.upload {
			display: flex;
			flex-direction: column;
			justify-content: center;
			align-items: center;
			gap: 10px;
			height: 200px;
			border-radius: 10px;
			border: 2px dashed #555;
			cursor: pointer;
			color: var(--text);
			transition: background .2s ease-in-out, border .2s ease-in-out;
		}

		.upload:hover {
			border-color: #858585;
		}

		.upload>span {
			font-size: 20px;
			font-weight: bold;
		}

		.upload>input {
			width: 350px;
			max-width: 100%;
			padding: 5px;
			border: 1px solid #555;
		}
	</style>
<?php }
