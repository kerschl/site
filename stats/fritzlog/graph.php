<?php
include $_SERVER['DOCUMENT_ROOT'] . "/src/main.php";
$app = new App("stats", "pmke.de/stats/fritzlog/graph");
if ($app->check_user_login()) {
	date_default_timezone_set("Europe/Berlin");
	$sql = "SELECT MapKey, MapContent FROM MapNumeric Where MapTable = 'fritzlog';";
	$boxes = mysqli_query($app->database(), $sql);

	$today = new DateTime();
	date_time_set($today, 23, 59, 59);
	$svg = "";
	$styles = "<style>"; // not optimal, todo: colors from database
	$legend = "";
	$dates = "";
	if (mysqli_num_rows($boxes) > 0) {
		$rows = 21;
		$days = 20;
		$widthcol = 50;
		$heightcol = 20;
		include $_SERVER['DOCUMENT_ROOT'] . "/src/detectmobilebrowser.php";
		if ($mobilebrowser) {
			$days = 8;
		}
		if ($_GET) {
			$rows = $_GET['rows'] + 1;
			$days = $_GET['days'];
			$widthcol = $_GET['widthcol'];
			$heightcol = $_GET['heightcol'];
		}
		$width = $days * $widthcol;
		$height = $rows * $heightcol;
		$offsetlines = 5;
		$overgrowthlines = 2;
		for ($i = 0; $i < $rows; $i++) {
			$xl = $widthcol / 2 - $offsetlines - $overgrowthlines;
			$y = $heightcol * ($i + 1) - $heightcol / 2;
			$svg = $svg . '<line x1="' . $xl . '" x2="' . $width - $xl . '" y1="' . $y . '" y2="' . $y . '"></line>';
			$number = $rows - $i - 1;
			if ($number >= 10) {
				$xt = ($xl - 12) / 2; // 12 = width 2 character text
			} else {
				$xt = ($xl - 6) / 2; // 6 = width text
			}
			$svg = $svg . '<text x="' . $xt . '" y="' . $y + 4 . '">' . $number . '</text>'; // 4 = height text / 2
			$svg = $svg . '<text x="' . $width - $xl + $xt . '" y="' . $y + 4 . '">' . $number . '</text>';
		}
		$styles = $styles . '.date{width: ' . $widthcol . 'px;}'; // set width date
		$x = $widthcol / 2 - $offsetlines;
		$y = $heightcol / 2 - $overgrowthlines;
		$svg = $svg . '<line x1="' . $x . '" y1="' . $y . '" x2="' . $x . '" y2="' . $height - $y . '"></line>';
		$svg = $svg . '<line x1="' . $width - $x . '" y1="' . $y . '" x2="' . $width - $x . '" y2="' . $height - $y . '"></line>';
		while ($box = mysqli_fetch_array($boxes)) { // loop boxes
			$class = "color-" . $box["MapKey"];
			$styles = $styles . '.' . $class . ':before{background:var(--' . $class . ');}.' . $class . '{stroke:var(--' . $class . ');}';
			$legend = $legend . '<div class="box"><span onclick="togglePolyline(\'' . $class . '\')" class="' . $class . '">' . $box["MapContent"] . '</span></div>';
			$p = new Polyline();
			$p->set_class($class);
			for ($i = 0; $i < $days; $i++) {
				$p->data_push($rows * $heightcol - $heightcol / 2); // init dataset
			}
			$sql = "SELECT LogDateTime FROM FritzLog Where FritzBox = '" . $box["MapKey"] . "' AND LogDateTime > cast((now() - interval " . $days . " day) as date);";
			$fritzlog = mysqli_query($app->database(), $sql); // get data for each box
			if (mysqli_num_rows($fritzlog) > 0) {
				while ($log = mysqli_fetch_array($fritzlog)) {
					$date = date_create($log["LogDateTime"]);
					$diff = date_diff($today, $date);
					$daysdiff = ($days - 1) - $diff->format("%a"); # 0 to number of days - 1
					if ($daysdiff >= 0) { # no false datasets
						$p->set_data($daysdiff, $p->get_data($daysdiff) - $heightcol); // edit dataset
					}
				}
			}
			$svg = $svg . $p->get_polyline($widthcol);
		}
		for ($i = $days; $i > 0; $i--) {
			$dates = $dates . '<span class="date">' . date("d.m.", mktime(date("H"), date("i"), date("s"), date("m"), date("d") - $i + 1)) . '</span>';
		}
	} else {
		die("<p>no logs found</p>");
	}
	echo $styles; // not optimal, todo: colors from database
	include "styles.css";
	echo "</style>";
?>
	<header>
		<h1>pmke.de/stats/fritzlog/graph</h1>
		<hr>
	</header>
	<div class="graph mx-auto" style="width: <?= $width ?>px">
		<div class="legend">
			<?= $legend ?>
		</div>
		<div class="statistic">
			<div class="data">
				<svg style="width: <?= $width ?>px; height: <?= $height ?>px">
					<?= $svg ?>
				</svg>
			</div>
		</div>
		<div class="dates">
			<?= $dates ?>
		</div>
	</div>
	<aside>
		<p>graph displays dropouts per day</p>
		<p><a href="/stats/fritzlog/select.php">edit graph</a></p>
		<p><a href="/stats/">stats</a></p>
	</aside>
	<script>
		function togglePolyline(color) {
			el = document.querySelectorAll("polyline." + color)[0];
			if (el.style.display == "none") {
				el.style.display = "initial"
			} else {
				el.style.display = "none"
			}
		}
	</script>
<?php
}

class Polyline
{
	private $data = array();
	private $class;
	function data_push(int $val)
	{
		array_push($this->data, $val);
	}
	function set_data(int $id, int $val)
	{
		$this->data[$id] = $val;
	}
	function get_data(int $id)
	{
		return $this->data[$id];
	}
	function set_class(string $class)
	{
		$this->class = $class;
	}
	function get_polyline($widthcol)
	{
		$str = '<polyline class="' . $this->class . '" points="';
		$int = $widthcol / 2;
		foreach ($this->data as $point) {
			$str = $str . $int . ',' . $point . ' ';
			$int = $int + $widthcol;
		}
		return $str . '"></polyline>';
	}
}
