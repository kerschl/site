<?php
include $_SERVER['DOCUMENT_ROOT'] . "/src/main.php";
$app = new App("stats", "pmke.de/stats/fritzlog/select");
if ($app->check_user_login()) {
?>
	<article>
		<h1>pmke.de/stats/fritzlog/select</h1>
		<hr>
		<p>select what you want to see</p>
		<form action="graph.php" method="get">
			<p>
				<label for="rows">rows</label>
				<input type="number" id="rows" name="rows" min="10" max="30" value="20">
			</p>
			<p>
				<label for="days">days</label>
				<input type="number" id="days" name="days" min="7" max="30" value="20">
			</p>
			<p>
				<label for="widthcol">widthcol</label>
				<select name="widthcol" id="widthcol">
					<option value="40">40</option>
					<option value="50" selected>50</option>
					<option value="60">60</option>
				</select>
			</p>
			<p>
				<label for="heightcol">heightcol</label>
				<select name="heightcol" id="heightcol">
					<option value="15">15</option>
					<option value="20" selected>20</option>
					<option value="25">25</option>
					<option value="30">30</option>
				</select>
			</p>
			<button type="submit">make it so</button>
		</form>
	</article>
<?php
}
