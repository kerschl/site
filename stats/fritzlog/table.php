<?php
include $_SERVER['DOCUMENT_ROOT'] . "/src/main.php";
$app = new App("stats", "pmke.de/stats/fritzlog/table");
if ($app->check_user_login()) {
?>
	<article>
		<h1>pmke.de/stats/fritzlog/table</h1>
	</article>
	<table>
		<?php
		$sql = "SELECT `FritzLog`.`IpAddress`, `FritzLog`.`LogDateTime`, `MapNumeric`.`MapContent` " .
			"FROM `FritzLog` INNER JOIN `MapNumeric` ON `FritzLog`.`FritzBox` = `MapNumeric`.`MapKey` " .
			"Where `MapNumeric`.`MapTable` = 'fritzlog' ORDER BY `FritzLog`.`LogDateTime` DESC";
		$logs = mysqli_query($app->database(), $sql);
		if (mysqli_num_rows($logs) > 0) {
			echo "<tr><th>Date/Time</th><th>Fritzbox</th><th>IP Address</th></tr>";
			while ($log = mysqli_fetch_array($logs)) {
				echo "<tr><td>" . $log["LogDateTime"] .
					"</td><td>" . $log["MapContent"] .
					"</td><td>" . long2ip($log["IpAddress"]) .
					"</td></tr>";
			}
		} ?>
	</table>
	<div class="mx-auto w-standard">
		<p><a href="/stats/">stats</a></p>
	</div>
<?php }
