<?php
include $_SERVER['DOCUMENT_ROOT'] . "/src/main.php";
$app = new App("stats", "pmke.de/stats/fritzlog/table2");
if ($app->check_user_login()) {
?>
	<article>
		<h1>pmke.de/stats/fritzlog/table2</h1>
	</article>
	<table>
		<?php
		$sql = "SELECT DATE_FORMAT(LogDateTime, '%H') as Stunde, COUNT(*) as Ausfaelle FROM FritzLog GROUP BY Stunde";
		$logs = mysqli_query($app->database(), $sql);
		if (mysqli_num_rows($logs) > 0) {
			echo "<tr><th>Stunde</th><th>Ausf&auml;lle</th></tr>";
			while ($log = mysqli_fetch_array($logs)) {
				echo "<tr><td>" . $log["Stunde"] .
					"</td><td>" . $log["Ausfaelle"] .
					"</td></tr>";
			}
		} ?>
	</table>
	<div class="mx-auto w-standard">
		<p><a href="/stats/">stats</a></p>
	</div>
<?php }
