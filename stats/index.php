<?php
include $_SERVER['DOCUMENT_ROOT'] . "/src/main.php";
$app = new App("stats", "pmke.de/stats");
if ($app->check_user_login()) {
?>
	<article>
		<h1>pmke.de/stats</h1>
		<hr />
		<p><a href="/stats/fritzlog/graph.php">fritzlog graph</a></p>
		<p><a href="/stats/fritzlog/table.php">fritzlog table</a></p>
		<p><a href="/stats/fritzlog/table2.php">fritzlog table2</a></p>
		<p><a href="/">index</a></p>
		<p><a href="/src/auth.php">logout</a></p>
	</article>
<?php }
