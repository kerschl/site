<?php
include $_SERVER['DOCUMENT_ROOT'] . "/src/main.php";
$app = new Site("designed-to-last", "This Page is Designed to Last");
?>
<article>
	<h1><?= $app->get_app_title() ?></h1>
	<hr />
	<p>
		<span>I observe how the quality of the Internet is handicapped by various design practices and therefore support initiatives such as the following:</span>
	</p>
	<p>
		<span><a href="https://jeffhuang.com/designed_to_last/" target="_blank">Jeff Huang's "This Page is Designed to Last"</a></span>
	</p>
	<p>
		<span><a href="https://www.reddit.com/r/programming/comments/ed88ra/this_page_is_designed_to_last_a_manifesto_for/" target="_blank">The corresponding reddit thread</a></span>
	</p>
	<hr>
	<blockquote>
		<p>
			<i>Jeff Huang's manifesto on "This Page is Designed to Last" emphasizes the importance of preserving web content by advocating for simpler, more sustainable web design practices. He reflects on the rapid decay of web links and the challenges of maintaining content online due to complex technologies and platform dependencies. Huang proposes seven guidelines to ensure web content longevity, including using vanilla HTML/CSS, avoiding HTML minimization, preferring single-page websites, ending hotlinking, sticking with native fonts, compressing images, and eliminating broken URL risks. These principles aim to make websites easier to maintain and more accessible for future generations.</i>
		</p>
	</blockquote>
	<hr>
	<p>
		I try to design my websites with this principle in mind. I make a fundamental distinction between webapps, that change with changing requirements, and websites, that are designed for the long term.
	</p>
	<hr>
	<p>
		<a href="/">index</a>
	</p>
</article>
