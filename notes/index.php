<?php
include $_SERVER['DOCUMENT_ROOT'] . "/src/main.php";
$app = new App("notes", "Notes");
if ($app->check_user_login()) {
	if (isset($_POST['send'])) {
		if ($_POST['send'] == 'delete') {
			$sql = "DELETE FROM `Note` WHERE `NoteID` = " . $_POST['id'] . ";";
		} else {
			$note = mysqli_real_escape_string($app->database(), htmlspecialchars($_POST['note']));
			if ($_POST['send'] == 'update') {
				$sql = "UPDATE `Note` SET `Content` = '" . $note . "', `UpdateUser` = '" . $_SESSION["UserID"] .
					"', `Updated` = now() WHERE `NoteID` = " . $_POST['id'] . ";";
			} else if ($_POST['send'] == 'add') {
				$sql = "INSERT INTO `Note` (`Content`, `CreateUser`, `UpdateUser`) VALUES ('" . $note .
					"','" . $_SESSION["UserID"] . "','" . $_SESSION["UserID"] . "');";
			}
		}
		if (!mysqli_query($app->database(), $sql)) {
			echo "Error: " . $sql . "<br>" . mysqli_error($app->database());
		}
	} ?>
	<article>
		<h1>Notes</h1>
	</article>
	<table>
		<?php
		$notes = mysqli_query($app->database(), "SELECT * FROM `Note`");
		if (mysqli_num_rows($notes) > 0) {
			while ($note = mysqli_fetch_array($notes)) { ?>
				<tr>
					<td>
						<form action="/notes/" method="post">
							<input type="hidden" name="id" value="<?php echo $note["NoteID"]; ?>" />
							<textarea id="note<?php echo $note["NoteID"]; ?>" name="note" rows="4" oninput="this.style.height = ''; this.style.height = this.scrollHeight +'px'"><?= htmlspecialchars_decode($note["Content"]) ?></textarea>
							<div class="flex">
								<button type="submit" name="send" value="delete" onclick="return confirm('Do you really want to delete this note?')" tabindex="-1">DELETE</button>
								<button type="submit" name="send" value="update">UPDATE</button>
							</div>
						</form>
					</td>
					<td class="mobile-hidden">
						<p>created:<br><?= $note["Created"]; ?></p>
						<p>updated:<br><?= $note["Updated"]; ?></p>
					</td>
				</tr>
		<?php }
		} else {
			echo '<tr><td><p>no notes found</p></td></tr>';
		}
		?>
		<tr>
			<td>
				<form action="/notes/" method="post">
					<label for="newnote">new:</label><br>
					<textarea id="newnote" name="note" rows="3" cols="50"></textarea>
					<div class="flex">
						<button name="reset" type="reset" tabindex="-1">CLEAR</button>
						<button type="submit" name="send" value="add">ADD</button>
					</div>
				</form>
			</td>
			<td class="mobile-hidden">
				<p><a href="/apps/">apps</a></p>
				<p><a href="/">index</a></p>
				<p><a href="/src/auth.php">logout</a></p>
			</td>
		</tr>
	</table>
<?php }
