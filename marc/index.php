<?php
include $_SERVER['DOCUMENT_ROOT'] . "/src/main.php";
$app = new App("marc", "Marc's Wohnsitze");
?>
<article>
	<h1><?= $app->get_app_title() ?></h1>
</article>
<table>
	<tr>
		<td>(Bundes-) Land</td>
		<td>Wohnsitz</td>
		<td>Zeitraum</td>
	</tr>
	<tr>
		<td>Bayern</td>
		<td>Geboren in Starnberg</td>
		<td>07.07.2001</td>
	</tr>
	<tr>
		<td>Bayern</td>
		<td>Traunstein</td>
		<td>??</td>
	</tr>
	<tr>
		<td>Bayern</td>
		<td>München</td>
		<td>??</td>
	</tr>
	<tr>
		<td>China</td>
		<td>Shanghai</td>
		<td>??</td>
	</tr>
	<tr>
		<td>Saarland</td>
		<td>Scheidt</td>
		<td>??</td>
	</tr>
	<tr>
		<td>Saarland</td>
		<td>Scheidt</td>
		<td>??</td>
	</tr>
	<tr>
		<td>Saarland</td>
		<td>St. Ingbert</td>
		<td>??</td>
	</tr>
	<tr>
		<td>Egypt</td>
		<td>Kairo</td>
		<td>201X - 201X</td>
	</tr>
	<tr>
		<td>Bayern</td>
		<td>Lambach</td>
		<td>201X - 201X</td>
	</tr>
	<tr>
		<td>Bayern</td>
		<td>Prien</td>
		<td>201X - 201X</td>
	</tr>
	<tr>
		<td>Vereinigte Arabische Emirate</td>
		<td>Abu Dhabi 1</td>
		<td>201X - 201X</td>
	</tr>
	<tr>
		<td>Vereinigte Arabische Emirate</td>
		<td>Abu Dhabi 2</td>
		<td>201X - 201X</td>
	</tr>
	<tr>
		<td>Bayern</td>
		<td>Grassau</td>
		<td>201X - 201X</td>
	</tr>
	<tr>
		<td>Nordrhein-Westfalen</td>
		<td>Essen</td>
		<td>201X - 201X</td>
	</tr>
	<tr>
		<td>Nordrhein-Westfalen</td>
		<td>Velbert</td>
		<td>201X - 201X</td>
	</tr>
	<tr>
		<td>Baden Württemberg</td>
		<td>Gingen an der Fils</td>
		<td>201X - Nov 2021</td>
	</tr>
	<tr>
		<td>Baden Württemberg</td>
		<td>Göppingen ??</td>
		<td>201X - 201X</td>
	</tr>
	<tr>
		<td>Baden Württemberg</td>
		<td>Göppingen Willhelm-Busch-Weg 9</td>
		<td>201X - Nov 2021</td>
	</tr>
	<tr>
		<td>Baden Württemberg</td>
		<td>Göppingen Ulrichstraße 17</td>
		<td>Nov 2021 - Juli 2022</td>
	</tr>
	<tr>
		<td>Nordrhein-Westfalen</td>
		<td>Donsbrüggen</td>
		<td>Juli 2022 - November 2023</td>
	</tr>
	<tr>
		<td>Nordrhein-Westfalen</td>
		<td>Kleve</td>
		<td>Juli 2022 - November 2023</td>
	</tr>
	<tr>
		<td>Bayern</td>
		<td>Marquartstein</td>
		<td>November 2023 - ??</td>
	</tr>
</table>
