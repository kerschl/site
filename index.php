<?php
include $_SERVER['DOCUMENT_ROOT'] . "/src/main.php";
$app = new Site("index", "Peter Kerschl's Website");
?>
<article>
	<h1><?= $app->get_app_title() ?></h1>
	<hr />
	<p>Welcome to my website (en)<br />Willkommen auf meiner Website (de)</p>
	<p>
		<?php include $_SERVER['DOCUMENT_ROOT'] . "/assets/icons/external-link.svg"; ?>
		<a href="https://gitlab.com/kerschl" target="_blank">Gitlab</a>
	</p>
	<p>
		<?php include $_SERVER['DOCUMENT_ROOT'] . "/assets/icons/mail.svg"; ?>
		<span>mail</span><span>@</span><span>pmke.de</span>
	</p>
	<p>
		<span><a href="/designed-to-last">This page is designed to last</a></span>
	</p>
	<hr>
	<p>
		<a href="https://pmke.de/">pmke.de</a> &rarr;
		<u>P</u>eter <u>M</u>aximilian <u>Ke</u>rschl :)
	</p>
</article>
