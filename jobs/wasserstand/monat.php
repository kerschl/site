<?php
$currentMonth = date('m');
$currentYear = date('Y');

// Calculate the last month and year
$lastMonth = ($currentMonth == 1) ? 12 : $currentMonth - 1;
$lastYear = ($lastMonth == 12) ? $currentYear - 1 : $currentYear;
// Get the number of days in the last month
$lastMonthDays = cal_days_in_month(CAL_GREGORIAN, $lastMonth, $lastYear);

// dates as string
if ($lastMonth < 10) {
	$lastMonth = "0" . $lastMonth;
}
$firstDayLastMonth = "01.$lastMonth.$lastYear";
$lastDayLastMonth = "$lastMonthDays.$lastMonth.$lastYear";

$urlGraph = "https://www.hnd.bayern.de/webservices/graphik.php?statnr=18400503" .
	"&thema=hochwasser.seespiegel&wert=seewasserstand" .
	"&begin=" . $firstDayLastMonth .
	"&end=" . $lastDayLastMonth;

$urlWeb = "https://www.hnd.bayern.de/pegel/inn/stock-18400503?" .
	"&begin=" . $firstDayLastMonth .
	"&end=" . $lastDayLastMonth;

require $_SERVER['DOCUMENT_ROOT'] . '/src/mailping.php';

$body = '<img src="' . $urlGraph . '" alt="graph of last month"><br>Genauere Infos: <a href="' . $urlWeb . '">' . $urlWeb . '</a>';
MailPing("Entwicklung Wasserstand $firstDayLastMonth - $lastDayLastMonth", $body);
