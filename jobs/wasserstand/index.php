<?php
$response = file_get_contents('https://pmke.de/api/wasserstand/prien.json');
if ($response === false) {
	die('Error fetching data from the API');
}

$currentWaterlevelInCentimeters = intval(
	str_replace(
		',',
		'',
		json_decode($response, true)['data']['current']
	)
);

// for testing
// $currentWaterlevelInCentimeters = 51830;

$cachedWaterlevelInCentimeters = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/jobs/wasserstand/cache.txt');
if ($cachedWaterlevelInCentimeters == false) {
	exit;
}
file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/jobs/wasserstand/cache.txt', $currentWaterlevelInCentimeters);

if ($currentWaterlevelInCentimeters == $cachedWaterlevelInCentimeters) {
	exit;
}

require $_SERVER['DOCUMENT_ROOT'] . '/src/mailping.php';
$chiemseeNN = 51820; // in Centimeter ueber NN

// wenn nicht (cached wasserstand 1cm ueber CMNN) && wasserstand 1cm ueber CMNN
if (!($chiemseeNN < $cachedWaterlevelInCentimeters) && $chiemseeNN < $currentWaterlevelInCentimeters) {
	echo "Wasserstand ist über Chiemsee NN gestgigen<br/>";
	$body = "Der Wasserstand ist <b>&uuml;ber</b> Chiemsee Normalnull gestgigen<br>" .
		"Chiemsee Normalnull in Zentimetern: " . $chiemseeNN . "<br>" .
		"Neuer Wasserstand in Zentimetern: " . $currentWaterlevelInCentimeters;
	MailPing("Der Wasserstand ist ueber Chiemsee NN gestgigen", $body);
}

// wenn nicht (cached wasserstand 1cm unter CMNN) && wasserstand 1cm unter CMNN
if (!($chiemseeNN > $cachedWaterlevelInCentimeters) && $chiemseeNN > $currentWaterlevelInCentimeters) {
	echo "Wasserstand ist unter Chiemsee NN gefallen<br/>";
	$body = "Der Wasserstand ist <b>unter</b> Chiemsee NN gefallen<br>" .
		"Chiemsee Normalnull in Zentimetern: " . $chiemseeNN . "<br>" .
		"Neuer Wasserstand in Zentimetern: " . $currentWaterlevelInCentimeters;
	MailPing("Der Wasserstand ist unter Chiemsee NN gefallen", $body);
}
