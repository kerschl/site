<?php
include $_SERVER['DOCUMENT_ROOT'] . '/src/config/imap.php';
include $_SERVER['DOCUMENT_ROOT'] . '/src/config/sql.php';

try {
	$conn = mysqli_connect($sql_host, $sql_username, $sql_password, $sql_database);
} catch (mysqli_sql_exception $e) {
	die("SQL error: " . $e->getMessage());
}
$imap = imap_open("{" . $imap_host . "/imap/ssl}INBOX", $imap_username, $imap_password);
$mailbox = imap_check($imap);
$fetch = imap_fetch_overview($imap, "1:{$mailbox->Nmsgs}");
foreach ($fetch as $item) { # loop through all mails
	$search  = array(",", "*****SPAM***** ");
	$subject = explode(' ', str_replace($search, "", imap_mime_header_decode($item->subject)[0]->text));
	if ($subject[0] == 'Internet-Adresse:') { # if subject is
		$mailfrom = imap_mime_header_decode($item->from)[0]->text;
		if ($result = mysqli_query($conn, "SELECT `MapKey` FROM `MapNumeric` WHERE `MapTable` = 'fritzlog' AND `MapContent` = '" . $mailfrom . "';")) { # if sender is valid
			$ipaddress = ip2long($subject[1]);
			$unixTimestamp = strtotime($item->date);
			$logdatetime = date('Y-m-d H:i:s', $unixTimestamp);
			$mapkey = mysqli_fetch_assoc($result);
			$fritzbox = $mapkey['MapKey'];
			$sql = "SELECT COUNT(LogID) as Count FROM FritzLog WHERE IpAddress = '" . $ipaddress . "' AND LogDateTime = '" . $logdatetime . "' AND FritzBox = '" . $fritzbox . "';";
			if (intval(mysqli_fetch_assoc(mysqli_query($conn, $sql))['Count']) > 0) { # if data is already in the database
				imap_delete($imap, $item->uid, FT_UID);
			} else {
				$sql = "INSERT INTO FritzLog (IpAddress, LogDateTime, FritzBox, CreateUser, UpdateUser) VALUES ('" . $ipaddress . "','" . $logdatetime . "','" . $fritzbox . "',1,1);";
				mysqli_query($conn, $sql);
			}
		}
	}
}
imap_close($imap);
mysqli_close($conn);
