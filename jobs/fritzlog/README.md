<a name="readme-top"></a>
<br />

<div align="center">
  <a href="https://gitlab.com/kerschl">
    <img src="../../assets/logos/fritzlog.png" alt="Logo" width="80" height="80">
  </a>
  <h3 align="center">Jobs > FritzLog</h3>
  <p align="center">
    A script that saves fritzbox logs to a sql database
  </p>
</div>

## Functions

-   imap_open \_close - Open/close IMAP stream
-   imap_check - Check current mailbox
-   imap_fetch_overview - Fetches mail headers for the given sequence
-   imap_mime_header_decode - Decodes strange mail strings
-   imap_delete - Marks message for deletion
-   imap_expunge - Delete all messages marked for deletion
